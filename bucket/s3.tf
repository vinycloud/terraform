module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = var.bucket_name
  acl    = "private"

  versioning = {
    enabled = true
  }

  tags = {
    Name        = var.bucket_name
    Environment = "DEV"
    ManagedBy   = "Terraform"
    Owner       = "Vinicius Fialho"
  }

}