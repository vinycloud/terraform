module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  for_each = toset(["one",])

  name = "Ubuntu-${each.key}"

  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = "ubuntu-arm"
  monitoring             = true
  subnet_id              = var.subnet_id-b

  tags = {

    Environment = "PRD"
    ManagedBy = "Terraform"
    Owner = "Vinicius Fialho"
    
  }
}