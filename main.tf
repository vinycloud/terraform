terraform {
  #Terraform version required:
  required_version = "~> 1.0.11"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "terraform"
}

terraform {
  backend "s3" {
    bucket = "descomplicando-terraform-vinicius-tfstates"
    #dynamodb_table = "terraform-state-lock-dynamo"
    key     = "LINUXtips/terraform-test.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}
