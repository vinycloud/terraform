variable "image_id" {
  default     = "ami-04fe9398b2a27a600"
  type        = string
  description = "The id of the machine image (AMI) to use for the server."

  validation {
    condition     = length(var.image_id) > 4 && substr(var.image_id, 0, 4) == "ami-"
    error_message = "The image_id value must be a valid AMI id, starting with \"ami-\"."
  }
}

variable "instance_type" {
  default     = "t4g.micro"
  type        = string
  description = "New Instance Type t4g.micro ARM"
}

variable "key" {
  default = "ubuntu-arm"
}

variable "servers" {

}

variable "subnet_id-a" {
  default     = "subnet-013ef4a6847e219fc"
  type        = string
  description = "Subnet Public-01 - us-east-1a"

}

variable "subnet_id-b" {
  default     = "subnet-00a38b9c18532197d"
  type        = string
  description = "Subnet Public-01 - us-east-1b"
}

variable "securit_group" {
  default     = "sg-09da3fa84da8d9444"
  type        = string
  description = "Default SG"
}


