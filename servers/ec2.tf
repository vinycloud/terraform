data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-arm64-server-*"]
  }

  owners = ["099720109477"] # Imagem Ubuntu mais recente arquitetura ARM64
}

resource "aws_instance" "web" {

  count         = var.servers
  ami           = var.image_id
  instance_type = var.instance_type
  key_name      = aws_key_pair.key.key_name
  subnet_id     = var.subnet_id-a
  user_data     = file("servers/scripts/webserver.sh")

  tags = {
    Name        = "Ubuntu"
    Environment = "DEV"
    ManagedBy   = "Terraform"
    Owner       = "Vinicius Fialho"
  }
}

