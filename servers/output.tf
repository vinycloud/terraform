output "descriptions" {
  value = {

    public-ip  = aws_instance.web[*].public_ip
    private-ip = aws_instance.web[*].private_ip
    tags       = aws_instance.web[*].tags

  }
}