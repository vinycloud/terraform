resource "aws_key_pair" "key" {
  key_name   = "ubuntu"
  public_key = file("./servers/ubuntu.pub")
}
