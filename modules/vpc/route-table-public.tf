resource "aws_route_table" "public" {
  vpc_id = aws_vpc.my-vpc.id
}

resource "aws_route" "internet_gateway" {
  destination_cidr_block = "0.0.0.0/0"
  route_table_id = aws_route_table.public.id
  gateway_id = aws_internet_gateway.this.id
}

resource "aws_route_table_association" "public" {
  for_each = toset(local.public_subnets)
  subnet_id = aws_subnet.this[each.value].id
  route_table_id = aws_route_table.public.id
}

